"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.oneof = void 0;
const Arbitrary_1 = require("./definition/Arbitrary");
class OneOfArbitrary extends Arbitrary_1.Arbitrary {
    constructor(arbs) {
        super();
        this.arbs = arbs;
    }
    generate(mrng) {
        const id = mrng.nextInt(0, this.arbs.length - 1);
        return this.arbs[id].generate(mrng);
    }
    withBias(freq) {
        return new OneOfArbitrary(this.arbs.map((a) => a.withBias(freq)));
    }
}
function oneof(...arbs) {
    if (arbs.length === 0) {
        throw new Error('fc.oneof expects at least one parameter');
    }
    return new OneOfArbitrary([...arbs]);
}
exports.oneof = oneof;

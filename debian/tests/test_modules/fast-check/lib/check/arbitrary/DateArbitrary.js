"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.date = void 0;
const IntegerArbitrary_1 = require("./IntegerArbitrary");
function date(constraints) {
    const intMin = constraints && constraints.min !== undefined ? constraints.min.getTime() : -8640000000000000;
    const intMax = constraints && constraints.max !== undefined ? constraints.max.getTime() : 8640000000000000;
    if (Number.isNaN(intMin))
        throw new Error('fc.date min must be valid instance of Date');
    if (Number.isNaN(intMax))
        throw new Error('fc.date max must be valid instance of Date');
    if (intMin > intMax)
        throw new Error('fc.date max must be greater or equal to min');
    return IntegerArbitrary_1.integer(intMin, intMax).map((a) => new Date(a));
}
exports.date = date;

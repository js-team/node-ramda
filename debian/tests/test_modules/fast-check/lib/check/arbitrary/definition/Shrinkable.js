"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Shrinkable = void 0;
const Stream_1 = require("../../../stream/Stream");
const symbols_1 = require("../../symbols");
class Shrinkable {
    constructor(value_, shrink = () => Stream_1.Stream.nil()) {
        this.value_ = value_;
        this.shrink = shrink;
        this.hasToBeCloned = symbols_1.hasCloneMethod(value_);
        this.readOnce = false;
        Object.defineProperty(this, 'value', { get: this.getValue });
    }
    getValue() {
        if (this.hasToBeCloned) {
            if (!this.readOnce) {
                this.readOnce = true;
                return this.value_;
            }
            return this.value_[symbols_1.cloneMethod]();
        }
        return this.value_;
    }
    applyMapper(mapper) {
        if (this.hasToBeCloned) {
            const out = mapper(this.value);
            if (out instanceof Object) {
                out[symbols_1.cloneMethod] = () => mapper(this.value);
            }
            return out;
        }
        return mapper(this.value);
    }
    map(mapper) {
        return new Shrinkable(this.applyMapper(mapper), () => this.shrink().map((v) => v.map(mapper)));
    }
    filter(refinement) {
        const refinementOnShrinkable = (s) => {
            return refinement(s.value);
        };
        return new Shrinkable(this.value, () => this.shrink()
            .filter(refinementOnShrinkable)
            .map((v) => v.filter(refinement)));
    }
}
exports.Shrinkable = Shrinkable;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getGlobal = void 0;
const internalGlobalThis = (function (global) {
    return global.globalThis ? global.globalThis : global;
})(typeof this === 'object' ? this : Function('return this')());
const getGlobal = () => internalGlobalThis;
exports.getGlobal = getGlobal;
